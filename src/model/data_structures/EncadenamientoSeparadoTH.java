package model.data_structures;

import java.util.LinkedList;

public class EncadenamientoSeparadoTH<K,V> {
	
	private ListaLlaveValorSecuencial<K, V>[] tbl;
	private int m;
	private LinkedList<Integer> pop ;

	public EncadenamientoSeparadoTH(int M) {
		// TODO Auto-generated constructor stub
		m=M;
		tbl = new ListaLlaveValorSecuencial[m];
	}
	public int darTamanio(){
		return tbl.length;
	}
	public boolean estaVacia() {
		boolean rta=false;
		for (int i = 0; i < tbl.length; i++) {
			rta&=tbl[i].estaVacia();
		}
		return rta;
	}
	public boolean tieneLlave(K llave){
		boolean rta=false;
		for (int i = 0; i < tbl.length; i++) {
			rta&=tbl[i].tieneLlave(llave);
		}
		return rta;
	}
	public V darValor(K llave){
		V rta=null;
		for (int i = 0; i < tbl.length; i++) {
			if(tbl[i].darValor(llave)!=null){
				return tbl[i].darValor(llave);
			}
		}
		return rta;
	}
	public void insertar(K llave, V valor){
		int a = hash(llave);
		if(valor==null){
			if(tieneLlave(llave)){
				tbl[a].insertar(llave, null);
			}
		}
		else{
			tbl[a].insertar(llave, valor);
			
		}
	}

	private int hash(K llave){
		return (llave.hashCode() & 0x7fffffff) % m;
		
	}
	public int[] darLongitudListas(){
		int[] rta = new int[darTamanio()];
		for (int i = 0; i < tbl.length; i++) {
			rta[i] = tbl[i].darTamanio();
		}
		return rta;
	}
	public Iterable<K> llaves(){
		pop = new LinkedList<>();
		for (int i = 0; i < tbl.length; i++) 
		{
			pop.add(i);
		}
		return (Iterable<K>) pop.iterator();
	}

}
