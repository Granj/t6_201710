package model.logic;

import java.util.Random;


public class GeneradorDatos
{
	public String[] generarCadenas(int N) 
	{
		String[] ruta = new String[N];
		int num = 0;
		Random aleatorio = new Random();
		while (num < N) 
		{
			String temp = "";
			Random aleatAux = new Random();
			int pos = 0;
			int aux = aleatorio.nextInt(10);
			while (pos < aux) 
			{
				char simbolo = (char) aleatAux.nextInt(28);
				if ((simbolo >= '0' && simbolo <= '9') || (simbolo >= 'A' && simbolo <= 'Z')) 
				{
					temp = temp + simbolo;
					pos++;
				}
			}
			ruta[num] = temp;
			num++;
		}
		return ruta;
	}

	public Integer[] generarAgnos(int N) 
	{
		Integer[] ruta = new Integer[N];
		int pos = 0;
		Random aleatorio = new Random();
		while (pos < N)
		{
			ruta[pos] = (aleatorio.nextInt(68) + 1950);
			pos++;
		}
		return ruta;
	}

}
