package model.logic;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import com.google.gson.Gson;
import model.data_structures.EncadenamientoSeparadoTH;
import model.vo.VOInfoPelicula;

public class ManejadorPeliculas
{

	private EncadenamientoSeparadoTH< String , VOInfoPelicula> hashTable;


	public void  cargarJson( String pRuta)
	{
		Gson cargador = new Gson();
		try (Reader lector = new FileReader(pRuta)) 
		{
			VOInfoPelicula[] object = cargador.fromJson(lector, VOInfoPelicula[].class);
			for(VOInfoPelicula estractor : object)
				hashTable.insertar(estractor.getMovieId(), estractor );
		}
		catch (IOException e) 
		{
			System.out.println("Hubo un error al leer el archivo con la ruta" + pRuta );
		}
	}

	//  consultar el título, el año, el director, los actores y el rating Imdb
	public String consultarTitulo(String pMovieId)  
	{
		if( hashTable.estaVacia() || hashTable.tieneLlave(pMovieId))	
			return null;
		else
			return	hashTable.darValor(pMovieId).getTitulo();
	}
	public int consultarAnio(String pMovieId)  
	{
		if( hashTable.estaVacia() || hashTable.tieneLlave(pMovieId))	
			return 0;
		else
			return	hashTable.darValor(pMovieId).getAnio();
	}
	public String consultarDirector(String pMovieId)
	{
		if( hashTable.estaVacia() || hashTable.tieneLlave(pMovieId))	
			return null;
		else
			return	hashTable.darValor(pMovieId).getDirector();
	}
	public String consultarActores(String pMovieId)
	{
		if( hashTable.estaVacia() || hashTable.tieneLlave(pMovieId))	
			return null;
		else
			return	hashTable.darValor(pMovieId).getActores();
	}
	public double consultarRatingIMDB(String pMovieId)
	{
		if( hashTable.estaVacia() || hashTable.tieneLlave(pMovieId))	
			return 0;
		else
			return	hashTable.darValor(pMovieId).getRatingIMDB();
	}
	
}
