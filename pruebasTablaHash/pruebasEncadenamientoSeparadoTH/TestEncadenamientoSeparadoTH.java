package pruebasEncadenamientoSeparadoTH;

import java.util.ArrayList;

import javafx.scene.control.Tab;
import junit.framework.TestCase;
import model.data_structures.EncadenamientoSeparadoTH;
import model.logic.GeneradorDatos;
import model.vo.VOInfoPelicula;

public class TestEncadenamientoSeparadoTH<K, V>extends TestCase {

	private ArrayList<VOInfoPelicula> datos;
	private String[] a;
	private Integer[] b;
	

	public void setUpEscenario1(){
		a = (new GeneradorDatos().generarCadenas(12));
		b = (new GeneradorDatos().generarAgnos(12));
		datos = new ArrayList<VOInfoPelicula>();
		for (int i = 0; i < datos.size(); i++) 
		{
			((model.vo.VOInfoPelicula) datos.get(i)).setTitulo(a[i]);
			((model.vo.VOInfoPelicula) datos.get(i)).setAnio(b[i]);
		}
	}
	public void TestInsertar(){
		setUpEscenario1();
		EncadenamientoSeparadoTH<String,VOInfoPelicula> TablaHash = new EncadenamientoSeparadoTH<String,VOInfoPelicula>(12);
		for (int i = 0; i < datos.size(); i++) {
			TablaHash.insertar(a[i], datos.get(i));
		}
		assertTrue(TablaHash.tieneLlave(a[2]));
	}
	public void TestHash(){
		setUpEscenario1();
		EncadenamientoSeparadoTH<String,VOInfoPelicula> TablaHash = new EncadenamientoSeparadoTH<String,VOInfoPelicula>(12);
		for (int i = 0; i < datos.size(); i++) {
			TablaHash.insertar(a[i], datos.get(i));
		}
		Iterable<K> c = (Iterable<K>) TablaHash.llaves();
		for (K k : c) {
			for (int i = 0; i < this.a.length; i++) {
				if(k.equals(a[i].hashCode())){
					assertEquals(true, true);
					break;
				}
			}
		}
		assertEquals(false, true);
	}
	public void testdarValor(){
		setUpEscenario1();
		EncadenamientoSeparadoTH<String,VOInfoPelicula> TablaHash = new EncadenamientoSeparadoTH<String,VOInfoPelicula>(12);
		for (int i = 0; i < datos.size(); i++) {
			TablaHash.insertar(a[i], datos.get(i));
		}
		assertEquals(TablaHash.darValor(a[2]), datos.get(2));
	}
	public void TestEstaVacia(){
		setUpEscenario1();
		EncadenamientoSeparadoTH<String,VOInfoPelicula> TablaHash = new EncadenamientoSeparadoTH<String,VOInfoPelicula>(12);
		for (int i = 0; i < datos.size(); i++) {
			TablaHash.insertar(a[i], datos.get(i));
		}
		assertEquals(false, TablaHash.estaVacia());
	}
}
